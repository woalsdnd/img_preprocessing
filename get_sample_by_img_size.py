from sets import Set
import argparse
import os
from PIL import Image
import numpy as np
from subprocess import Popen, PIPE
import pandas as pd


def mkdir_if_not_exist(out_dir):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)


def image_shape(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    img_shape = img_arr.shape
    return img_shape


def image2arr(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    return img_arr


def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [fname for fname in os.listdir(path)]
        else:
            filenames = [fname for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--img_dir',
    type=str,
    required=False
    )
parser.add_argument(
    '--out_dir',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

mkdir_if_not_exist(FLAGS.out_dir)

cold_start = False

if cold_start:
    list_img_size = []
    filenames = all_files_under(FLAGS.img_dir)
    for filename in filenames:
        img = image2arr(filename)
        img_shape = img.shape
        if not img_shape in list_img_size:
            list_img_size.append(img_shape)
            out_path = os.path.join(FLAGS.out_dir, "{}_{}.jpeg".format(img_shape[0], img_shape[1]))
            pipes = Popen(["cp", filename, out_path], stdout=PIPE, stderr=PIPE)
            std_out, std_err = pipes.communicate()
else:
    for n_system in range(1, 5):
        df = pd.read_csv("/media/ext/csv_files/filename_size_system{}.csv".format(n_system))
        df.drop_duplicates(subset=["height", "width"], inplace=True)
        for fname in list(df["filename"]):
            fpath = os.path.join("/media/ext/tiff/system{}/".format(n_system), fname)
            img = image2arr(fpath)
            img_shape = img.shape
            out_path = os.path.join(FLAGS.out_dir, "{}_{}.jpeg".format(img_shape[0], img_shape[1]))
            pipes = Popen(["cp", fpath, out_path], stdout=PIPE, stderr=PIPE)
            std_out, std_err = pipes.communicate()
        
