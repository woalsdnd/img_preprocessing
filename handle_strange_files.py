import pandas as pd
import os
from subprocess import Popen, PIPE

df = pd.read_csv("foreground_ratio_resized_img.csv")

for fpath in df[df["non_black_ratio"] < 0.4]["path"]:
    if "Gyeongido" not in os.path.basename(fpath):
        pipes = Popen(["cp", os.path.join("strange_imgs/resized/", os.path.basename(fpath)), fpath], stdout=PIPE, stderr=PIPE)
        std_out, std_err = pipes.communicate()
        pipes = Popen(["cp", fpath, "/media/ext/resized/strange_imgs"], stdout=PIPE, stderr=PIPE)
        std_out, std_err = pipes.communicate()

#     pipes = Popen(["mv", os.path.join("/media/ext/resized/strange_imgs", os.path.basename(fpath)), fpath], stdout=PIPE, stderr=PIPE)
#     std_out, std_err = pipes.communicate()
#     if os.path.exists(fpath):
#         pipes = Popen(["mv", fpath, "/media/ext/resized/strange_imgs"], stdout=PIPE, stderr=PIPE)
#         std_out, std_err = pipes.communicate()