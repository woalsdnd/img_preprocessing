import argparse
import os
import multiprocessing

from PIL import Image
from scipy.misc import imresize

import numpy as np

def image_shape(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    img_shape = img_arr.shape
    return img_shape

def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [os.path.basename(fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames

def images2arrs(filenames):
    img_shape = image_shape(filenames[0])
    images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1], img_shape[2]), dtype=np.float32)
    for file_index in xrange(len(filenames)):
        img = Image.open(filenames[file_index])
        img.load()
        images_arr[file_index] = np.asarray(img).astype(np.float32)
    
    return images_arr

def process(args):
    filenames,out_dir, h_target, w_target=args
    for filename in filenames:
        # skip processed files
        out_file=os.path.join(out_dir,os.path.basename(filename))
        if os.path.exists(out_file):
            continue
        
        # pad & resize
        img=images2arrs([filename])[0]
        img_h, img_w, _ =img.shape
        max_len=max(img.shape[0],img.shape[1])
        padded=np.zeros((max_len,max_len,3))
        padded[(max_len-img_h)//2:(max_len-img_h)//2+img_h,(max_len-img_w)//2:(max_len-img_w)//2+img_w,...]=img
        resized_img=imresize(padded,(h_target,w_target),'bicubic')
#         resized_img=imresize(padded,(h_target,w_target),'bilinear')

        # save the image
        Image.fromarray(resized_img.astype(np.uint8)).save(out_file)

def resize_images(img_dir,out_dir,n_processes,h_target, w_target):
    
    # get the list of filenames
    if not os.path.isdir(img_dir):
        raise Exception("Folder name " + img_dir + " does not exist")
    
    # make out_dir if not exists
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    
    # divide files
    all_files=all_files_under(img_dir)
    filenames = [[] for __ in xrange(n_processes)]
    chunk_sizes = len(all_files) // n_processes
    for index in xrange(n_processes):
        if index == n_processes - 1:   # allocate ranges (last GPU takes remainders)
            start, end = index * chunk_sizes, len(all_files)
        else:
            start, end = index * chunk_sizes, (index + 1) * chunk_sizes
        filenames[index] = all_files[start:end]
    
    # run multiple processes
    pool = multiprocessing.Pool(processes=FLAGS.n_processes)
    try:
        pool.map(process, [(filenames[i], out_dir,h_target,w_target) for i in range(FLAGS.n_processes)])
    except:
        print "Error: unable to start multiprocessing" 
    
    
if __name__ == '__main__':
    parser=argparse.ArgumentParser()
    parser.add_argument(
        '--img_dir',
        type=str,
        help="Directory of input image files",
        required=True
        )
    parser.add_argument(
        '--out_dir',
        type=str,
        help="Directory of output image files",
        required=True
        )
    parser.add_argument(
        '--n_processes',
        type=int,
        help="Directory of output image files",
        required=True
        )
    parser.add_argument(
        '--target_w',
        type=int,
        help="Target width",
        required=True
        )
    parser.add_argument(
        '--target_h',
        type=int,
        help="Target height",
        required=True
        )
    FLAGS,_= parser.parse_known_args()
    
    resize_images(FLAGS.img_dir,FLAGS.out_dir,FLAGS.n_processes,FLAGS.target_h,FLAGS.target_w)
