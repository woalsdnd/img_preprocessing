import os
import numpy as np
from PIL import Image
from scipy.misc import imresize

# img_size = 512
img_size = 1024
resample_method = "bicubic"

list_fpath = ["/media/ext/ODIR/ODIR-5K_Training_Dataset/2133_right.jpg", "/media/ext/ODIR/ODIR-5K_Training_Dataset/2134_left.jpg",
              "/media/ext/ODIR/ODIR-5K_Training_Dataset/1209_left.jpg", "/media/ext/ODIR/ODIR-5K_Training_Dataset/4522_left.jpg"]

for fpath in list_fpath:
    img = np.array(Image.open(fpath))
    h_ori, w_ori, _ = img.shape
    len_side = min(h_ori, w_ori)
    crop_img = img[h_ori // 2 - len_side // 2:h_ori // 2 + len_side // 2, w_ori // 2 - len_side // 2:w_ori // 2 + len_side // 2]
    max_len = max(crop_img.shape[0], crop_img.shape[1])
    img_h, img_w, _ = crop_img.shape
    padded = np.zeros((max_len, max_len, 3), dtype=np.uint8)
    padded[
        (max_len - img_h) // 2:
        (max_len - img_h) // 2 + img_h,
        (max_len - img_w) // 2:
        (max_len - img_w) // 2 + img_w, ...] = crop_img
    resized_img = imresize(padded, (img_size, img_size), resample_method)
    Image.fromarray(resized_img).save("/media/ext/ODIR/ODIR-5K_Training_Dataset_resized_{}_{}/{}.png".format(img_size, img_size, os.path.basename(fpath).replace(".jpg", "")))
