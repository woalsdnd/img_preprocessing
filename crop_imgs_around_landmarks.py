# crop images around landmarks detected by 512x512 after resizeing height to 1024 maintaining the ratio
# option : --n_processes
# output : cropped images at /media/ext/cropped_around_landmark/system*
import argparse

from PIL import Image

import numpy as np
import os
import multiprocessing

import pandas as pd
from scipy.misc import imresize

img_dir_template="/media/ext/tiff/system{}"
out_dir_template="/media/ext/cropped_around_landmark/system{}"
landmark_csv_template="/media/ext/landmark_coords_in_original_imgs/system{}.csv"

h_target_resize=1024
crop_size=(512,512)

def pad_imgs(imgs, img_size):
    img_h,img_w=imgs.shape[1], imgs.shape[2]
    target_h,target_w=img_size[0],img_size[1]
    if len(imgs.shape)==4:
        padded=np.zeros((imgs.shape[0], target_h, target_w, imgs.shape[3]))
    elif len(imgs.shape)==3:
        padded=np.zeros((imgs.shape[0], target_h, target_w))
    padded[:,(target_h-img_h)//2:(target_h-img_h)//2+img_h,(target_w-img_w)//2:(target_w-img_w)//2+img_w,...]=imgs
    
    return padded

def process(args):
    filenames,out_dir, df=args
    df=df[df.filename.isin(filenames)]
    for row in df.itertuples(index=True):
        filename=row.filename

        # skip processed files
        out_file_disc=os.path.join(out_dir,"disc",os.path.basename(filename))
        if os.path.exists(out_file_disc):
            continue
        out_file_macula=os.path.join(out_dir,"macula",os.path.basename(filename))
        if os.path.exists(out_file_macula):
            continue
        
        # resize image and adjust points
        img=image2arr(filename)
        img_shape = img.shape
        h,w=img_shape[0],img_shape[1]
        w2h_ratio=1.*w/h
        rescale_ratio=1.*h_target_resize/h
        w_target_resize=int(h_target_resize*w2h_ratio)
        resized_img=imresize(img,(h_target_resize,w_target_resize),'bilinear')
        disc_y_new,disc_x_new,fovea_y_new,fovea_x_new=map(int,rescale_ratio*np.array((row.disc_y, row.disc_x, row.fovea_y,row.fovea_x)))
        
        # crop and pad image to 512x512 if necessary
        disc_cropped = resized_img[np.maximum(0,disc_y_new-crop_size[0]/2):disc_y_new+crop_size[0]/2, 
                          disc_x_new-crop_size[1]/2:disc_x_new+crop_size[1]/2,...]
        fovea_cropped = resized_img[np.maximum(0,fovea_y_new-crop_size[0]/2):fovea_y_new+crop_size[0]/2, 
                          fovea_x_new-crop_size[1]/2:fovea_x_new+crop_size[1]/2,...]
        disc_cropped_padded=pad_imgs(np.expand_dims(disc_cropped, axis=0), crop_size)
        fovea_cropped_padded=pad_imgs(np.expand_dims(fovea_cropped, axis=0), crop_size)
        
        # save cropped image
        Image.fromarray(disc_cropped_padded[0,...].astype(np.uint8)).save(out_file_disc)
        Image.fromarray(fovea_cropped_padded[0,...].astype(np.uint8)).save(out_file_macula)

def image_shape(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    img_shape = img_arr.shape
    return img_shape

def image2arr(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    return img_arr

def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [fname for fname in os.listdir(path)]
        else:
            filenames = [fname for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames

# arrange arguments
parser=argparse.ArgumentParser()
parser.add_argument(
    '--n_processes',
    type=int,
    help="Directory of output image files",
    required=True
    )
FLAGS,_= parser.parse_known_args()

for i in range(1,5):
    # make out_dir if not exists
    img_dir=img_dir_template.format(i)
    out_dir=out_dir_template.format(i)
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    if not os.path.isdir(os.path.join(out_dir,"disc")):
        os.makedirs(os.path.join(out_dir,"disc"))
    if not os.path.isdir(os.path.join(out_dir,"macula")):
        os.makedirs(os.path.join(out_dir,"macula"))   
         
    # divide files
    df=pd.read_csv(landmark_csv_template.format(i))
    all_files=all_files_under(img_dir)
    filenames = [[] for __ in xrange(FLAGS.n_processes)]
    chunk_sizes = len(all_files) // FLAGS.n_processes
    for index in xrange(FLAGS.n_processes):
        if index == FLAGS.n_processes - 1:   # allocate ranges (last GPU takes remainders)
            start, end = index * chunk_sizes, len(all_files)
        else:
            start, end = index * chunk_sizes, (index + 1) * chunk_sizes
        filenames[index] = all_files[start:end]
    
    # run multiple processes
    pool = multiprocessing.Pool(processes=FLAGS.n_processes)
    try:
        pool.map(process, [(filenames[i], out_dir, df) for i in range(FLAGS.n_processes)])
    except:
        print "Error: unable to start multiprocessing" 
    