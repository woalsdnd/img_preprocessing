# python simple_resize.py  --img_dir=PALM-Training400/ --out_dir=preprocessed/PALM-Training400  --n_processes=30 --w_target=512 --h_target=512 --extension="jpg"
import argparse

from PIL import Image

import numpy as np
import os
import multiprocessing
from skimage import measure
from scipy.misc import imresize


def replace_img_format(file_basename, extension):
    file_basename = file_basename.replace("." + extension, ".png")
    return file_basename

    
def process(args):
    h_target, w_target, filenames, out_dir, extension = args
    for filename in filenames:
        # if "jpg" in filename:
            # os.system("rm {}".format(os.path.join(out_dir, replace_img_format(os.path.basename(filename), "jpg"))))
        # skip processed files
        out_file = os.path.join(out_dir, replace_img_format(os.path.basename(filename), extension))
        if os.path.exists(out_file):
            continue
        
        # read the image and resize 
        img = np.array(Image.open(filename))
        resized_img = imresize(img, (h_target, w_target), 'bicubic')
        
        # save cropped image
        Image.fromarray(resized_img).save(out_file)

def image_shape(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    img_shape = img_arr.shape
    return img_shape


def image2arr(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    return img_arr


def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [fname for fname in os.listdir(path)]
        else:
            filenames = [fname for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--img_dir',
    type=str,
    help="directory of original images",
    required=True
    )
parser.add_argument(
    '--out_dir',
    type=str,
    help="output directory of cropped images",
    required=True
    )
parser.add_argument(
    '--n_processes',
    type=int,
    help="Directory of output image files",
    required=True
    )
parser.add_argument(
    '--w_target',
    type=int,
    help="Target width",
    required=True
    )
parser.add_argument(
    '--h_target',
    type=int,
    help="Target height",
    required=True
    )
parser.add_argument(
    '--extension',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# make out_dir if not exists
if not os.path.isdir(FLAGS.out_dir):
    os.makedirs(FLAGS.out_dir)

# divide files
all_files = all_files_under(FLAGS.img_dir)
filenames = [[] for __ in xrange(FLAGS.n_processes)]
chunk_sizes = len(all_files) // FLAGS.n_processes
for index in xrange(FLAGS.n_processes):
    if index == FLAGS.n_processes - 1:  # allocate ranges (last GPU takes remainders)
        start, end = index * chunk_sizes, len(all_files)
    else:
        start, end = index * chunk_sizes, (index + 1) * chunk_sizes
    filenames[index] = all_files[start:end]

# run multiple processes
pool = multiprocessing.Pool(processes=FLAGS.n_processes)
pool.map(process, [(FLAGS.h_target, FLAGS.w_target, filenames[i], FLAGS.out_dir, FLAGS.extension) for i in range(FLAGS.n_processes)])
