from PIL import Image
import numpy as np
import os
import pandas as pd
import multiprocessing
import argparse
from uuid import uuid4
import SharedArray

def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [os.path.basename(fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames

def process(args):
    index, filenames, shared_array_name, start_index=args
    array = SharedArray.attach(shared_array_name)
    for i, fn in enumerate(filenames):
        img=Image.open(fn)
        arr=np.array(img)
        shape=arr.shape
        index=start_index+i
        array[index,0]=shape[0]
        array[index,1]=shape[1]
        
# arrange arguments
parser=argparse.ArgumentParser()
parser.add_argument(
    '--n_processes',
    type=int,
    help="Directory of output image files",
    required=True
    )
FLAGS,_= parser.parse_known_args()

tiff_dir="/media/ext/external_data/messidor/cropped_imgs/"
for n_system in range(5,6):
    # divide files
    all_filenames=all_files_under(tiff_dir)
    n_total=len(all_filenames)
    filenames = [[] for __ in range(FLAGS.n_processes)]
    start_indices=[[] for __ in range(FLAGS.n_processes)]
    chunk_sizes = len(all_filenames) // FLAGS.n_processes
    for index in range(FLAGS.n_processes):
        if index == FLAGS.n_processes - 1:   # allocate ranges (last GPU takes remainders)
            start, end = index * chunk_sizes, len(all_filenames)
        else:
            start, end = index * chunk_sizes, (index + 1) * chunk_sizes
        start_indices[index]=start
        filenames[index] = all_filenames[start:end]
    
    # run multiple processes
    try:
        pool=multiprocessing.Pool(processes=FLAGS.n_processes)
        shared_array_name = str(uuid4())
        shared_array = SharedArray.create(shared_array_name, [n_total,2], dtype=np.float32)
        pool.map(process, [(i, filenames[i], shared_array_name, start_indices[i]) for i in range(FLAGS.n_processes)])
        arr=np.array(shared_array, dtype=np.float32)
        heights=arr[:,0].tolist()
        widths=arr[:,1].tolist()
    
    finally:
        SharedArray.delete(shared_array_name)
    
    all_filenames=all_files_under(tiff_dir, append_path=False)
    df=pd.DataFrame({'filename': all_filenames,'height': heights, 'width' : widths})
    df.to_csv("/media/ext/csv_files/filename_size_system{}.csv".format(n_system),index=False)
