for sys in 1_1 1_2 1_3 1_4 2_1 2_2 2_3 2_4 2_5 2_6 2_7 2_8 2_9 2_10 2_11 2_12 2_13 2_14 3_1 3_2 3_3 3_4 3_5 3_6 3_7 4_1 4_2 4_3 4_4 4_5; do
    echo "extracting images from dicoms for [system${sys}]..." ;
    python extract_images_from_dicom.py --dicom_dir=/media/ext/dicom/system${sys}/ --image_dir=/media/ext/tiff/system$(echo $sys | cut -d "_" -f1) --converted_dicom_dir=/media/ext/converted_dicoms/converted_dicom_system${sys} --n_processes=20 ;
done

for sys in 1 2 3 4; do
    echo "cropping images for [system${sys}]..." ;
    python crop_center.py --img_dir=/media/ext/tiff/system${sys} --out_dir=/media/ext/cropped/system${sys} --n_processes=20;
 	echo "resizing images for [system${sys}]..." ;
    python resize_images.py --img_dir=/media/ext/cropped/system${sys} --out_dir=/media/ext/resized/system${sys} --n_processes=20 --target_w=512 --target_h=512 ;
done
