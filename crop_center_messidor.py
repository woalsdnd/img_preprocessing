# crop center to be width/height = 1
import argparse

from PIL import Image

import numpy as np
import os
import multiprocessing

ratio_w2h_cropped=1.0
    
def process(args):
    filenames, out_dir=args
    for filename in filenames:
        # skip processed files
        out_file=os.path.join(out_dir,os.path.basename(filename))
        if os.path.exists(out_file):
            continue
        
        # crop image
        img=image2arr(filename)
        img_shape = img.shape
        assert len(img_shape)==3
        x_center=img_shape[1]/2
        height=img_shape[0]
        cropped_img = img[:, int(x_center-ratio_w2h_cropped/2*height):int(x_center+ratio_w2h_cropped/2*height),...]
          
        # save cropped image
        Image.fromarray(cropped_img).save(out_file)

def image_shape(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    img_shape = img_arr.shape
    return img_shape

def image2arr(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    return img_arr

def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [fname for fname in os.listdir(path)]
        else:
            filenames = [fname for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames

# arrange arguments
parser=argparse.ArgumentParser()
parser.add_argument(
    '--img_dir',
    type=str,
    help="directory of original images",
    required=True
    )
parser.add_argument(
    '--out_dir',
    type=str,
    help="output directory of cropped images",
    required=True
    )
parser.add_argument(
    '--n_processes',
    type=int,
    help="Directory of output image files",
    required=True
    )
FLAGS,_= parser.parse_known_args()

# make out_dir if not exists
if not os.path.isdir(FLAGS.out_dir):
    os.makedirs(FLAGS.out_dir)

# divide files
all_files=all_files_under(FLAGS.img_dir)
filenames = [[] for __ in xrange(FLAGS.n_processes)]
chunk_sizes = len(all_files) // FLAGS.n_processes
for index in xrange(FLAGS.n_processes):
    if index == FLAGS.n_processes - 1:   # allocate ranges (last GPU takes remainders)
        start, end = index * chunk_sizes, len(all_files)
    else:
        start, end = index * chunk_sizes, (index + 1) * chunk_sizes
    filenames[index] = all_files[start:end]

# run multiple processes
pool=multiprocessing.Pool(processes=FLAGS.n_processes)
try:
    pool.map(process, [(filenames[i], FLAGS.out_dir) for i in range(FLAGS.n_processes)])
except:
    print "Error: unable to start multiprocessing" 
