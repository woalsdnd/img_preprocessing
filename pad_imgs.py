import os

from PIL import Image
import numpy as np

target_img_size = (640, 640)

path_training_data = "../data/training/original_images"
path_test_data = "../data/test/original_images"
path_training_artery_mask = "../data/training/original_artery/"
path_training_vein_mask = "../data/training/original_vein/"
path_test_artery_mask = "../data/test/original_artery/"
path_test_vein_mask = "../data/test/original_vein/"


def pad_imgs(imgs, img_size, is_batch=True):
    """
    Pad a batch of images with the shape of (N, h, w, d) or (N, h, w) if is_batch=True,
        an image with the shape of (h, w, d) or (h, w), otherwise
    
    Args:
        imgs (numpy array): a batch of images with the shape of (N, h, w, d) or (N, h, w)
        img_size (tuple): (target_h, target_w)
    
    Returns:
        padded images (numpy array)
     
    """
    if is_batch:
        assert len(imgs.shape) == 3 or  len(imgs.shape) == 4 
        img_h, img_w = imgs.shape[1], imgs.shape[2]
        target_h, target_w = img_size[0], img_size[1]
        if len(imgs.shape) == 4:
            d = imgs.shape[3]
            padded = np.zeros((imgs.shape[0], target_h, target_w, d))
        elif len(imgs.shape) == 3:
            padded = np.zeros((imgs.shape[0], img_size[0], img_size[1]))
        padded[:, (target_h - img_h) // 2:(target_h - img_h) // 2 + img_h, (target_w - img_w) // 2:(target_w - img_w) // 2 + img_w, ...] = imgs
    else:
        assert len(imgs.shape) == 2 or  len(imgs.shape) == 3 
        img_h, img_w = imgs.shape[0], imgs.shape[1]
        target_h, target_w = img_size[0], img_size[1]
        if len(imgs.shape) == 3:
            padded = np.zeros((target_h, target_w, imgs.shape[2]))
        elif len(imgs.shape) == 2:
            padded = np.zeros((img_size[0], img_size[1]))
        padded[(target_h - img_h) // 2:(target_h - img_h) // 2 + img_h, (target_w - img_w) // 2:(target_w - img_w) // 2 + img_w, ...] = imgs
    return padded


def makedirs(dir_path):
    if not os.path.isdir(dir_path):
        os.makedirs(dir_path)


for path_data in [path_training_data, path_test_data, path_training_artery_mask, path_training_vein_mask, path_test_artery_mask, path_test_vein_mask]:
    filepaths = [os.path.join(path_data, fname) for fname in os.listdir(path_data)]
    for filepath in filepaths: 
        img = np.array(Image.open(filepath))
        img = pad_imgs(img, target_img_size, is_batch=False)
        path_save = os.path.join(os.path.dirname(path_data), os.path.basename(path_data).replace("original_", ""))
        makedirs(path_save)
        Image.fromarray(img.astype(np.uint8)).save(os.path.join(path_save, os.path.basename(filepath)))
