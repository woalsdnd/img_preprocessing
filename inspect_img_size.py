from sets import Set
import argparse
import os
from PIL import Image
import numpy as np

def image_shape(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    img_shape = img_arr.shape
    return img_shape

def image2arr(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    return img_arr

def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [fname for fname in os.listdir(path)]
        else:
            filenames = [fname for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames

# arrange arguments
parser=argparse.ArgumentParser()
parser.add_argument(
    '--img_dir',
    type=str,
    required=True
    )
FLAGS,_= parser.parse_known_args()

image_sizes=Set()  
filenames=all_files_under(FLAGS.img_dir)
for filename in filenames:
    try:
        img=image2arr(filename)
    except:
        continue
    img_shape=img.shape
    image_sizes.add(img_shape)
print image_sizes