# python crop_resize.py  --img_dir=PALM-Training400/ --out_dir=preprocessed/PALM-Training400  --n_processes=30 --w_target=512 --h_target=512 --extension="jpg"
import argparse

from PIL import Image

import numpy as np
import os
import multiprocessing
from skimage import measure
from scipy.misc import imresize


def replace_img_format(file_basename, extension):
    file_basename = file_basename.replace("." + extension, ".png")
    return file_basename

    
def process(args):
    h_target, w_target, filenames, out_dir, extension = args
    for filename in filenames:
        # skip processed files
        out_file = os.path.join(out_dir, replace_img_format(os.path.basename(filename), extension))
        if os.path.exists(out_file):
            continue
#         print(filename)
        
        # read the image and resize
        im = Image.open(filename)
        img = np.array(im)
        if img.shape[2] == 4:  # alpha channel
            img = img[..., :3]
        h_ori, w_ori, _ = np.shape(img)
        red_threshold = 20
        roi_check_len = h_ori // 5
        
        # Find Connected Components with intensity above the threshold
        blobs_labels, n_blobs = measure.label(img[:, :, 0] > red_threshold, return_num=True)
        if n_blobs == 0:
            print ("crop failed for %s " % (filename)
                    + "[no blob found]")
            continue
        
        # Find the Index of Connected Components of the Fundus Area (the central area)
        majority_vote = np.argmax(np.bincount(blobs_labels[h_ori // 2 - roi_check_len // 2:h_ori // 2 + roi_check_len // 2,
                                                                w_ori // 2 - roi_check_len // 2:w_ori // 2 + roi_check_len // 2].flatten()))
        if majority_vote == 0:
            print ("crop failed for %s " % (filename)
                    + "[invisible areas (intensity in red channel less than " + str(red_threshold) + ") are dominant in the center]")
            continue
        
        row_inds, col_inds = np.where(blobs_labels == majority_vote)
        row_max = np.max(row_inds)
        row_min = np.min(row_inds)
        col_max = np.max(col_inds)
        col_min = np.min(col_inds)
        if row_max - row_min < 100 or col_max - col_min < 100:
            print n_blobs
            for i in range(1, n_blobs + 1):
                print len(blobs_labels[blobs_labels == i])
            print ("crop failed for %s " % (filename)
                    + "[too small areas]")
            continue
        
        # crop the image 
        crop_img = img[row_min:row_max, col_min:col_max]
        max_len = max(crop_img.shape[0], crop_img.shape[1])
        img_h, img_w, _ = crop_img.shape
        padded = np.zeros((max_len, max_len, 3), dtype=np.uint8)
        padded[(max_len - img_h) // 2:(max_len - img_h) // 2 + img_h, (max_len - img_w) // 2:(max_len - img_w) // 2 + img_w, ...] = crop_img
        resized_img = imresize(padded, (h_target, w_target), 'bicubic')
        
        # save cropped image
        Image.fromarray(resized_img).save(out_file)


def image_shape(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    img_shape = img_arr.shape
    return img_shape


def image2arr(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    return img_arr


def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [fname for fname in os.listdir(path)]
        else:
            filenames = [fname for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--img_dir',
    type=str,
    help="directory of original images",
    required=True
    )
parser.add_argument(
    '--out_dir',
    type=str,
    help="output directory of cropped images",
    required=True
    )
parser.add_argument(
    '--n_processes',
    type=int,
    help="Directory of output image files",
    required=True
    )
parser.add_argument(
    '--w_target',
    type=int,
    help="Target width",
    required=True
    )
parser.add_argument(
    '--h_target',
    type=int,
    help="Target height",
    required=True
    )
parser.add_argument(
    '--extension',
    type=str,
    help="Target height",
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# make out_dir if not exists
if not os.path.isdir(FLAGS.out_dir):
    os.makedirs(FLAGS.out_dir)

# divide files
all_files = all_files_under(FLAGS.img_dir)
filenames = [[] for __ in xrange(FLAGS.n_processes)]
chunk_sizes = len(all_files) // FLAGS.n_processes
for index in xrange(FLAGS.n_processes):
    if index == FLAGS.n_processes - 1:  # allocate ranges (last GPU takes remainders)
        start, end = index * chunk_sizes, len(all_files)
    else:
        start, end = index * chunk_sizes, (index + 1) * chunk_sizes
    filenames[index] = all_files[start:end]

# run multiple processes
pool = multiprocessing.Pool(processes=FLAGS.n_processes)
pool.map(process, [(FLAGS.h_target, FLAGS.w_target, filenames[i], FLAGS.out_dir, FLAGS.extension) for i in range(FLAGS.n_processes)])
