# nohup python simple_resize.py --img_dir=/media/ext/cropped/system1/ --out_dir=/media/ext/resized_128x128/system1 --n_processes=30 --w_target=128 --h_target=128 --extension="png" > out_system1_128x128 &
# nohup python simple_resize.py --img_dir=/media/ext/cropped/system2/ --out_dir=/media/ext/resized_128x128/system2 --n_processes=30 --w_target=128 --h_target=128 --extension="png" > out_system2_128x128 &
# nohup python simple_resize.py --img_dir=/media/ext/cropped/system3/ --out_dir=/media/ext/resized_128x128/system3 --n_processes=30 --w_target=128 --h_target=128 --extension="png" > out_system3_128x128 &
# nohup python simple_resize.py --img_dir=/media/ext/cropped/system4/ --out_dir=/media/ext/resized_128x128/system4 --n_processes=30 --w_target=128 --h_target=128 --extension="png" > out_system4_128x128 &

# nohup python simple_resize.py --img_dir=/media/ext/cropped/system1/ --out_dir=/media/ext/resized_256x256/system1 --n_processes=30 --w_target=256 --h_target=256 --extension="png" > out_system1_256x256 &
# nohup python simple_resize.py --img_dir=/media/ext/cropped/system2/ --out_dir=/media/ext/resized_256x256/system2 --n_processes=30 --w_target=256 --h_target=256 --extension="png" > out_system2_256x256 &
# nohup python simple_resize.py --img_dir=/media/ext/cropped/system3/ --out_dir=/media/ext/resized_256x256/system3 --n_processes=30 --w_target=256 --h_target=256 --extension="png" > out_system3_256x256 &
# nohup python simple_resize.py --img_dir=/media/ext/cropped/system4/ --out_dir=/media/ext/resized_256x256/system4 --n_processes=30 --w_target=256 --h_target=256 --extension="png" > out_system4_256x256 &

# nohup python simple_resize.py --img_dir=/media/ext/cropped/system1/ --out_dir=/media/ext/resized_512x512/system1 --n_processes=30 --w_target=512 --h_target=512 --extension="png" > out_system1_512x512 &
# nohup python simple_resize.py --img_dir=/media/ext/cropped/system2/ --out_dir=/media/ext/resized_512x512/system2 --n_processes=30 --w_target=512 --h_target=512 --extension="png" > out_system2_512x512 &
# nohup python simple_resize.py --img_dir=/media/ext/cropped/system3/ --out_dir=/media/ext/resized_512x512/system3 --n_processes=30 --w_target=512 --h_target=512 --extension="png" > out_system3_512x512 &
# nohup python simple_resize.py --img_dir=/media/ext/cropped/system4/ --out_dir=/media/ext/resized_512x512/system4 --n_processes=30 --w_target=512 --h_target=512 --extension="png" > out_system4_512x512 &

nohup python simple_resize.py --img_dir=../fovea_localization_20200506/data/AMD/ori_images/ --out_dir=../fovea_localization_20200506/data/AMD/128x128 --n_processes=30 --w_target=128 --h_target=128 --extension="png" > out_AMD_128x128 &
nohup python simple_resize.py --img_dir=../fovea_localization_20200506/data/AMD/ori_images/ --out_dir=../fovea_localization_20200506/data/AMD/256x256 --n_processes=30 --w_target=256 --h_target=256 --extension="png" > out_AMD_256x256 &

nohup python simple_resize.py --img_dir=../fovea_localization_20200506/data/PALM/ori_images/ --out_dir=../fovea_localization_20200506/data/PALM/128x128 --n_processes=30 --w_target=128 --h_target=128 --extension="png" > out_PALM_128x128 &
nohup python simple_resize.py --img_dir=../fovea_localization_20200506/data/PALM/ori_images/ --out_dir=../fovea_localization_20200506/data/PALM/256x256 --n_processes=30 --w_target=256 --h_target=256 --extension="png" > out_PALM_256x256 &

nohup python simple_resize.py --img_dir=../fovea_localization_20200506/data/REFUGE/ori_images/ --out_dir=../fovea_localization_20200506/data/REFUGE/128x128 --n_processes=30 --w_target=128 --h_target=128 --extension="png" > out_REFUGE_128x128 &
nohup python simple_resize.py --img_dir=../fovea_localization_20200506/data/REFUGE/ori_images/ --out_dir=../fovea_localization_20200506/data/REFUGE/256x256 --n_processes=30 --w_target=256 --h_target=256 --extension="png" > out_REFUGE_256x256 &
