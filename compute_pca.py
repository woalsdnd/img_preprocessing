import numpy as np
import os
from PIL import Image
from scipy.misc import imresize

def imagefiles2arrs(filenames):
    # convert to z score per image
    pixels=[]
    for file_index in xrange(len(filenames)):
        rgb=[[] for _ in range(3)]
        img = np.array(Image.open(filenames[file_index]))
        img=imresize(img, (img.shape[0]//8, img.shape[1]//8),'bilinear')
        for i in range(3):
            rgb[i]=np.expand_dims(img[...,i][img[...,0]>10].astype(np.float64), axis=1)
        rgb=np.concatenate(rgb, axis=1)
        pixels.append(rgb)
    
    pixels=np.concatenate(pixels, axis=0)
    pixels-=np.mean(pixels,axis=0)
    
    return pixels

def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [os.path.basename(fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


parent_dir="/media/ext/tiff"
bs = 1000
Us, evs = [], []
for index in range(1,5):
    target_dir=os.path.join(parent_dir,"system{}".format(index))
    filenames=all_files_under(target_dir, extension=".tiff")
    batches = [filenames[i * bs : (i + 1) * bs] for i in range(int(len(filenames) / bs) + 1)]
    for batch in batches:
        X = imagefiles2arrs(batch)
        cov = np.dot(X.T, X) / X.shape[0]
        U, S, V = np.linalg.svd(cov)
        ev = np.sqrt(S)
        Us.append(U)
        evs.append(ev)
        print ev
        print U

print('U')
print(np.mean(Us, axis=0))
print('eigenvalues')
print(np.mean(evs, axis=0))
