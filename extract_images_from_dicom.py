import argparse
import os
from subprocess import Popen, PIPE
import multiprocessing
import dicom

def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [fname for fname in os.listdir(path)]
        else:
            filenames = [fname for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames

def process(args):
    filenames,out_dir=args
    for filename in filenames:
        try:
            dicom_file = dicom.read_file(filename)
            out_file=os.path.join(out_dir, "{}.tiff".format(os.path.basename(filename)))
            if os.path.exists(out_file):
                continue
            pipes = Popen(["convert", filename, out_file], stdout=PIPE, stderr=PIPE)
            std_out, std_err = pipes.communicate()
            if len(std_err)>0:
                splitted=std_err.split("\n")
                if "convert.im6: Unsupported JPEG process: SOF type 0xc3" in splitted[0]:
                    converted_dicom_filename=os.path.join(FLAGS.converted_dicom_dir, os.path.basename(filename).rstrip(".dcm")+"_converted.dcm")
                    pipes = Popen(["gdcmconv", "-w",filename, converted_dicom_filename],
                                    stdout=PIPE, stderr=PIPE)
                    std_out, std_err = pipes.communicate()
                    if len(std_err)>0:
                        print splitted[0]
                        print "gdcmconv failed" 
                        print filename
                    pipes = Popen(["convert", converted_dicom_filename, out_file],
                            stdout=PIPE, stderr=PIPE)
                    std_out, std_err = pipes.communicate()
                    if len(std_err)>0:
                        print "convert failed after gdcmconv"
                else:
                    print splitted[0]
                    print "unknown error"
                    print filename     
        except:
            with open("failed_files","a") as f:
                f.write("{}\n".format(filename))


if __name__ == '__main__':
#     arrange arguments
    parser=argparse.ArgumentParser()
    parser.add_argument(
        '--dicom_dir',
        type=str,
        help="Directory of dicom files",
        required=True
        )
    parser.add_argument(
        '--image_dir',
        type=str,
        help="Directory of image files",
        required=True
        )
    parser.add_argument(
        '--converted_dicom_dir',
        type=str,
        help="Directory of converted dicom files",
        required=True
        )
    parser.add_argument(
        '--n_processes',
        type=int,
        help="Directory of converted dicom files",
        required=True
        )
    FLAGS,_= parser.parse_known_args()
    
    if not os.path.isdir(FLAGS.image_dir):
        os.makedirs(FLAGS.image_dir)
    if not os.path.isdir(FLAGS.converted_dicom_dir):
        os.makedirs(FLAGS.converted_dicom_dir)
    
    # divide files
    all_files=all_files_under(FLAGS.dicom_dir,extension=".dcm")
    filenames = [[] for __ in xrange(FLAGS.n_processes)]
    chunk_sizes = len(all_files) // FLAGS.n_processes
    for index in xrange(FLAGS.n_processes):
        if index == FLAGS.n_processes - 1:   # allocate ranges (last GPU takes remainders)
            start, end = index * chunk_sizes, len(all_files)
        else:
            start, end = index * chunk_sizes, (index + 1) * chunk_sizes
        filenames[index] = all_files[start:end]
    
    # run multiple processes
    pool = multiprocessing.Pool(processes=FLAGS.n_processes)
    try:
        pool.map(process, [(filenames[i], FLAGS.image_dir) for i in range(FLAGS.n_processes)])
    except:
        print "Error: unable to start multiprocessing" 
            
