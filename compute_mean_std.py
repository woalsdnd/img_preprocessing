import os
import numpy as np
from PIL import Image

def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [os.path.basename(fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames

def read_image(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    return img_arr

parent_dir="/media/ext/tiff"

sums=np.zeros(3)
squared_sums=np.zeros(3)
n=np.zeros(3)
for i in range(1,5):
    target_dir=os.path.join(parent_dir,"system{}".format(i))
    filenames=all_files_under(target_dir, extension=".tiff")
    for filename in filenames:
        img=read_image(filename)
        for i in range(3):
            n[i]+=len(img[...,i][img[...,0]>10])
            sums[i]+=np.sum(img[...,i][img[...,0]>10])
            squared_sums[i]+=np.sum(np.square(img[...,i][img[...,0]>10].tolist()))
    
avg=sums/n
avg_squred_sum=squared_sums/n
std=np.sqrt(avg_squred_sum-np.square(avg))
    
print avg, std
