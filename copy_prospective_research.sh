for task in CWP Drusen HardExudate Membrane RNFLDefect FluidAccumulation Hemorrhage MyelinatedNerveFiber RetinalPigmentaryChange ChroioretinalAtrophy GlaucomatousDiscChange MacularHole NonGlaucomatousDiscChange VascularAbnormality; do 
    echo "extracting images from dicoms for [${task}]..." ;
    python extract_images_from_dicom.py --dicom_dir=/media/ext/prospective_research/finding_non_overlapping/dcm_files/${task}_consensus3/ --image_dir=/media/ext/prospective_research/finding_non_overlapping/img_files/${task}_consensus3/ --converted_dicom_dir=/media/ext/prospective_research/finding_non_overlapping/converted_dcm_files/${task}_consensus3/ --n_processes=20 ;
done

#for task in normal abnormal; do 
#    echo "extracting images from dicoms for [system${task}]..." ;
#    python extract_images_from_dicom.py --dicom_dir=/media/ext/prospective_research/dcm_files/normal_abnormal/${task} --image_dir=/media/ext/prospective_research/img_files/normal_abnormal/${task} --converted_dicom_dir=/media/ext/prospective_research/converted_dcm_files/normal_abnormal/${task} --n_processes=20 ;
#done