# python compute_foreground_ratio_resized_img.py  --out_csv=foreground_ratio_resized_img.csv  
from sets import Set
import argparse
import os
from PIL import Image
import numpy as np
from subprocess import Popen, PIPE
import pandas as pd


def mkdir_if_not_exist(out_dir):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)


def image_shape(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    img_shape = img_arr.shape
    return img_shape


def image2arr(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    return img_arr


def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [fname for fname in os.listdir(path)]
        else:
            filenames = [fname for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--out_csv',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

LIST_EXCLUDING_SIZE = [(1280, 2000), (1344, 2000), (1376, 2000), (1408, 1840), (1408, 1856), (1424, 2000), (1456, 1808),
                       (1472, 1968), (1472, 2000), (1488, 2000), (1520, 2000), (1552, 2000), (1568, 1968), (1568, 2000),
                       (1584, 2000), (1600, 2000), (2120, 2979), (2279, 3202), (2308, 3319), (2520, 3541), (2521, 3542),
                       (1632, 2000), (1648, 2000), (1664, 2000), (1680, 2000), (1696, 2000), (1712, 2000), (1728, 2000),
                       (1744, 2000), (1760, 2000), (1776, 2000), (1792, 2000), (1824, 2000), (1840, 2000), (1856, 1616),
                       (1856, 2000), (1872, 1536), (1888, 2000), (1920, 2000), (1936, 2000), (1984, 2000), (2000, 1168),
                       (2000, 1520), (2000, 1648), (2000, 1712), (2000, 1792), (2000, 1808), (2000, 1840), (2000, 1856),
                       (2000, 1904), (2000, 1920), (2000, 1936), (2000, 1952), (2000, 1968), (2000, 2000), (2048, 2600),
                       (2544, 3575), (3072, 3072), (3072, 3900)]

list_img_path = []
list_img_non_black_ratio = []
for n_system in range(1, 5):
    img_dir = os.path.join("/media/ext/resized/system{}".format(n_system))
    filenames = all_files_under(img_dir)
    for filename in filenames:
        img_arr = image2arr(filename)
        img_shape = img_arr.shape
        n_black = len(img_arr[(img_arr[..., 0] < 10)])
        n_total = reduce(lambda x, y:x * y, img_arr.shape[:2])
        list_img_path.append(filename)
        list_img_non_black_ratio.append(1.*(n_total - n_black) / n_total)
            
pd.DataFrame({"path":list_img_path, "non_black_ratio":list_img_non_black_ratio}).to_csv(FLAGS.out_csv, index=False)
