import os

import numpy as np

from PIL import Image

path_training_data = "../data/training/av"
path_test_data = "../data/test/av"


def makedirs(dir_path):
    if not os.path.isdir(dir_path):
        os.makedirs(dir_path)


def generate_mask(arr, type):
    if type == "artery":  # red
        color_index = 0
    elif type == "vein":  # blue
        color_index = 2
    else:
        raise ValueError("type should be either artery or vein")
    # red or blue masks
    color_arr = np.zeros(arr.shape)
    color_arr[..., color_index] = 255
    mask = np.sum(arr == color_arr, axis=2)
    mask[mask == 3] = 255
    mask[mask < 3] = 0
    
    # green mask
    green_arr = np.zeros(arr.shape)
    green_arr[..., 1] = 255
    green_mask = np.sum(arr == green_arr, axis=2)
    mask[green_mask == 3] = 255
    return mask


for path_data in [path_training_data, path_test_data]:
    filepaths = [os.path.join(path_data, fname) for fname in os.listdir(path_data)]
    makedirs(os.path.join(os.path.dirname(path_data), "artery"))
    makedirs(os.path.join(os.path.dirname(path_data), "vein"))
    for filepath in filepaths:
        im = Image.open(filepath)
        arr = np.array(im)
        artery_mask = generate_mask(arr, "artery")
        vein_mask = generate_mask(arr, "vein")
        Image.fromarray(artery_mask.astype(np.uint8)).save(os.path.join(os.path.dirname(path_data), "artery", os.path.basename(filepath)))
        Image.fromarray(vein_mask.astype(np.uint8)).save(os.path.join(os.path.dirname(path_data), "vein", os.path.basename(filepath)))
